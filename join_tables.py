import pandas as pd

activity = pd.read_csv("api_activity.csv", low_memory=False)
dataproduct = pd.read_csv("api_dataproduct.csv", low_memory=False)
dataentity = pd.read_csv("api_dataentity.csv", low_memory=False)
observation = pd.read_csv("api_observation.csv", low_memory=False)

activity_generatedentities = pd.read_csv("api_activity_generatedEntities.csv",low_memory=False)

# step 1: merge data tables
full_dataproduct = pd.merge(dataproduct, dataentity, left_on="dataentity_ptr_id", right_on="entity_ptr_id")

# step 2: merge data tables with mapper
full_mapped_dataproduct = pd.merge(full_dataproduct, activity_generatedentities, left_on="dataentity_ptr_id", right_on="entity_id")

# step 3: merge the activities in
total_map = pd.merge(full_mapped_dataproduct, activity, left_on="activity_id", right_on="altaobject_ptr_id")

# step 4: merge the observation tables in
total_map_observations = pd.merge(total_map, observation, left_on="activity_id", right_on="activity_ptr_id")

# step 5: make data selections
# format == psrfits, endtime < 2020-01-01 (or startTime; in this case it is the same)
psrfits_map = total_map_observations.loc[total_map_observations["format"]=="psrfits"]
psrfits_2019_map = psrfits_map[psrfits_map['endTime'] < "2020-01-01"]
psrfits_operations_map = psrfits_2019_map[psrfits_2019_map['endTime'] > "2019-07-01"]
psrfits_operations_map.to_csv('arts-dr1.csv')

#api_activity.csv: startTime, endTime, locality_policy,
#api_dataproduct.csv: filesize, RA, DEC
#api_dataentity.csv: size, format
#to_be_derived: mimetype, exptime, obs_title
#api_dataproduct.csv: storageRef, dataProductType, dataProductSubType
#api_activity_generatedEntities.csv: link activity to entity
#dataentity_ptr_id (api_dataproduct.csv) = entity_ptr_id (api_dataentity.csv) = entity_id ( api_activity_generatedEntities.csv)
#altaobject_ptr_id (api_activity.csv) = activity_id ( api_activity_generatedEntities.csv)

