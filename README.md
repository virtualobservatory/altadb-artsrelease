# altadb-artsrelease
This direcotry contains two scripts.

First there is the `dumper` script. This script will make a list of all the tables in the database and dump each of those in a csv file with the same name. The script is executed as follows

```
./dumper /path/to/dir
```

where `/path/to/dir` is the *absolute* path to an existing directory where all the CSV files will be written.

Then there is the `join_tables.py` script that will read in the csv files and extract the data that is used by the ARTS DR1 data release in the VO. To execute it does not take any command line arguments.

This is all rather crude but it also is rather straightforward.
